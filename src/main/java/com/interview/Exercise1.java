package com.interview;

public class Exercise1 {

    /**
     * A palindrome is a word, number, phrase, or other sequence
     * of characters which reads the same backward as forward
     */
    public static boolean checkPalindrome(String text) {
        return false;
    }

    /**
     * f(0)=1
     * f(1)=1
     * f(n)=f(n-1) * n
     */
    public static int factorial(int inputNumber) {
        return Integer.parseInt(null);
    }

    /**
     * f(0)=0
     * f(1)=1
     * f(n)=f(n-1) + f(n-2)
     */
    public static int fibonacci(int element)  {
        return Integer.parseInt(null);
    }

}
