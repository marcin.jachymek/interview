package com.interview;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.interview.Exercise1.fibonacci;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FibonacciTest {

    /**
     * Implement Exception class for this test
     * and correct the test itself
     */
    @Test
    @DisplayName("fibonacci negative value")
    void check_fibonacci_negative() {
        Exception exception = new Exception();

        String expectedMessage = "provide exception message";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("fibonacci 0")
    void check_fibonacci_zero() {
        assertThat(fibonacci(0), is(0));
    }

    @Test
    @DisplayName("fibonacci 1")
    void check_fibonacci_one() {
        assertThat(fibonacci(1), is(1));
    }

    @Test
    @DisplayName("fibonacci 10")
    void check_factorial_ten() {
        assertThat(fibonacci(10), is(55));
    }

}
