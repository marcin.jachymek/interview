package com.interview;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.interview.Exercise2.sortAcsOnlyPositiveNumbers;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class SortAscOnlyPositiveTest {

    @Test
    @DisplayName("Sort ascending positive numbers in: [-1, 150, 190, 170, -1, -1, 160, 180]")
    void sort_positive_numbers_1() {
        assertArrayEquals(new int[]{-1, 150, 160, 170, -1, -1, 180, 190},
                sortAcsOnlyPositiveNumbers(new int[]{-1, 150, 190, 170, -1, -1, 160, 180}));
    }

    @Test
    @DisplayName("Sort ascending positive numbers in: [-1, -1, -1, -1, -1]")
    void sort_positive_numbers_2() {
        assertArrayEquals(new int[]{-1, -1, -1, -1, -1},
                sortAcsOnlyPositiveNumbers(new int[]{-1, -1, -1, -1, -1}));
    }

    @Test
    @DisplayName("Sort ascending positive numbers in: [-1]")
    void sort_positive_numbers_3() {
        assertArrayEquals(new int[]{-1},
                sortAcsOnlyPositiveNumbers(new int[]{-1}));
    }

    @Test
    @DisplayName("Sort ascending positive numbers in: [4, 2, 9, 11, 2, 16]")
    void sort_positive_numbers_4() {
        assertArrayEquals(new int[]{2, 2, 4, 9, 11, 16},
                sortAcsOnlyPositiveNumbers(new int[]{4, 2, 9, 11, 2, 16}));
    }

}
