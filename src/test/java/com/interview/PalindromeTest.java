package com.interview;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.interview.Exercise1.checkPalindrome;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class PalindromeTest {

    @Test
    @DisplayName("aabaa")
    void check_aabaa() {
        assertThat(checkPalindrome("aabaa"), is(true));
    }

    @Test
    @DisplayName("abac")
    void check_abac() {
        assertThat(checkPalindrome("abac"), is(false));
    }

    @Test
    @DisplayName("a")
    void check_a() {
        assertThat(checkPalindrome("a"), is(true));
    }

    @Test
    @DisplayName("az")
    void check_az() {
        assertThat(checkPalindrome("az"), is(false));
    }

    @Test
    @DisplayName("abacaba")
    void check_abacaba() {
        assertThat(checkPalindrome("abacaba"), is(true));
    }

    @Test
    @DisplayName("z")
    void check_z() {
        assertThat(checkPalindrome("z"), is(true));
    }

    @Test
    @DisplayName("aaabaaaa")
    void check_aaabaaaa() {
        assertThat(checkPalindrome("abac"), is(false));
    }

    @Test
    @DisplayName("zzzazzazz")
    void check_zzzazzazz() {
        assertThat(checkPalindrome("zzzazzazz"), is(false));
    }

    @Test
    @DisplayName("hlbeeykoqqqqokyeeblh")
    void check_hlbeeykoqqqqokyeeblh() {
        assertThat(checkPalindrome("hlbeeykoqqqqokyeeblh"), is(false));
    }


}
