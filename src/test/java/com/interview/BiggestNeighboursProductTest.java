package com.interview;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.interview.Exercise2.biggestNeighboursProduct;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BiggestNeighboursProductTest {

    /**
     * Implement Exception class for this test
     * and correct the test itself
     */
    @Test
    @DisplayName("array with zero elements")
    void check_not_meeting_requirements1() {
        Exception exception = new Exception();

        String expectedMessage = "provide exception message";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    /**
     * Exception should be the same as in check_not_meeting_requirements1
     * but slightly different message
     */
    @Test
    @DisplayName("array with one element")
    void check_not_meeting_requirements2() {
        Exception exception = new Exception();

        String expectedMessage = "provide exception message";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("Biggest neighbours product for [3, 6, -2, -5, 7, 3]")
    void check_biggest_neighbours_product_1() {
        assertThat(biggestNeighboursProduct(new int[]{3, 6, -2, -5, 7, 3}), is(21));
    }

    @Test
    @DisplayName("Biggest neighbours product for [-1, -2]")
    void check_biggest_neighbours_product_2() {
        assertThat(biggestNeighboursProduct(new int[]{-1, -2}), is(2));
    }

    @Test
    @DisplayName("Biggest neighbours product for [5, 1, 2, 3, 1, 4]")
    void check_biggest_neighbours_product_3() {
        assertThat(biggestNeighboursProduct(new int[]{5, 1, 2, 3, 1, 4}), is(6));
    }

    @Test
    @DisplayName("Biggest neighbours product for [1, 0, 1, 0, 1000]")
    void check_biggest_neighbours_product_4() {
        assertThat(biggestNeighboursProduct(new int[]{1, 0, 1, 0, 1000}), is(0));
    }

}
