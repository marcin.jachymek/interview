package com.interview;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.interview.Exercise1.factorial;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FactorialTest {

    /**
     * Implement Exception class for this test
     * and correct the test itself
     */
    @Test
    @DisplayName("factorial negative value")
    void check_factorial_negative() {
        Exception exception = new Exception();

        String expectedMessage = "provide exception message";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("factorial 0")
    void check_factorial_zero() {
        assertThat(factorial(0), is(1));
    }

    @Test
    @DisplayName("factorial 1")
    void check_factorial_one() {
        assertThat(factorial(1), is(1));
    }

    @Test
    @DisplayName("factorial 10")
    void check_factorial_ten() {
        assertThat(factorial(10), is(3628800));
    }

}
